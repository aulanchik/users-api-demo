"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchUser = exports.fetchUsers = exports.updateUser = exports.deleteUser = exports.createUser = void 0;
var User_1 = __importDefault(require("../models/User"));
var errors_1 = require("../helpers/errors");
var createUser = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var createdUser, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                // validate req.body before proceeding further with persisting new user
                if ((req.body.constructor === Object && !Object.keys(req.body).length) ||
                    (req.body.constructor === Array && !req.body.length)) {
                    throw new errors_1.ErrorHandler(404, "Request body should contain at least one user");
                }
                return [4 /*yield*/, User_1.default.create(req.body)];
            case 1:
                createdUser = _a.sent();
                return [2 /*return*/, res.status(201).send(createdUser)];
            case 2:
                error_1 = _a.sent();
                return [2 /*return*/, res.status(error_1.statusCode || 500).send(error_1.message)];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.createUser = createUser;
var deleteUser = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, fetchedUser, deletedUser, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User_1.default.findById(id)];
            case 2:
                fetchedUser = _a.sent();
                if (!fetchedUser) {
                    throw new errors_1.ErrorHandler(404, "User not found");
                }
                return [4 /*yield*/, User_1.default.findByIdAndRemove({ _id: id })];
            case 3:
                deletedUser = _a.sent();
                return [2 /*return*/, res.status(200).send(deletedUser)];
            case 4:
                error_2 = _a.sent();
                return [2 /*return*/, res.status(error_2.statusCode || 500).send(error_2.message)];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.deleteUser = deleteUser;
var updateUser = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, fetchedUser, updatedUser, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User_1.default.findById(id)];
            case 2:
                fetchedUser = _a.sent();
                if (!fetchedUser) {
                    throw new errors_1.ErrorHandler(404, "User not found");
                }
                return [4 /*yield*/, User_1.default.findByIdAndUpdate({ _id: id }, req.body, {
                        new: true,
                    })];
            case 3:
                updatedUser = _a.sent();
                return [2 /*return*/, res.status(200).send(updatedUser)];
            case 4:
                error_3 = _a.sent();
                return [2 /*return*/, res.status(error_3.statusCode || 500).send(error_3.message)];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.updateUser = updateUser;
var fetchUsers = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fetchedUser, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, User_1.default.find({})];
            case 1:
                fetchedUser = _a.sent();
                return [2 /*return*/, res.status(200).send(fetchedUser)];
            case 2:
                error_4 = _a.sent();
                return [2 /*return*/, res.status(error_4.statusCode || 500).send(error_4.message)];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.fetchUsers = fetchUsers;
var fetchUser = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, fetchedUser, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, User_1.default.findById(id)];
            case 2:
                fetchedUser = _a.sent();
                if (!fetchedUser) {
                    throw new errors_1.ErrorHandler(404, "User not found");
                }
                return [2 /*return*/, res.status(200).send(fetchedUser)];
            case 3:
                error_5 = _a.sent();
                return [2 /*return*/, res.status(error_5.statusCode || 500).send(error_5.message)];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.fetchUser = fetchUser;
