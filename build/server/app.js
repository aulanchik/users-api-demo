"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var errors_1 = require("./helpers/errors");
var index_1 = __importDefault(require("./routes/index"));
require("./database/connection");
require("./config");
var app = express_1.default();
app.use(express_1.default.json());
app.use(errors_1.errorMiddleware);
app.use("/", index_1.default);
exports.default = app;
