"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var index_1 = require("../controllers/index");
var router = express_1.Router();
router.route("/users").get(index_1.fetchUsers).post(index_1.createUser);
router.route("/users/:id").delete(index_1.deleteUser).get(index_1.fetchUser).patch(index_1.updateUser);
exports.default = router;
