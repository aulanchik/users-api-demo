import { Router } from "express";
import {
  createUser,
  deleteUser,
  fetchUsers,
  fetchUser,
  updateUser,
} from "../controllers/index";

const router = Router();

router.route("/users").get(fetchUsers).post(createUser);
router.route("/users/:id").delete(deleteUser).get(fetchUser).patch(updateUser);

export default router;
