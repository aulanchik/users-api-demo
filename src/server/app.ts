import express, { Express } from "express";
import { errorMiddleware } from "./helpers/errors";
import router from './routes/index'
import "./database/connection";
import "./config";

const app: Express = express();
app.use(express.json());
app.use(errorMiddleware);
app.use("/", router);

export default app;
