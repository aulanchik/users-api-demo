import app from "./app";

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Application server started at 127.0.0.1:${PORT}`);
});
