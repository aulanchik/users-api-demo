import mongoose from "mongoose";

/* To avoid deprecation warnings mentioned by mongoose when 
starting the database, we set these options according to the 
warning messages.If you want to get more information about 
this issue, visit this link below: 
https://mongoosejs.com/docs/connections.html#options.
*/
const options = {
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

let dbName = process.env.DEV_DB_NAME;
if (process.env.NODE_ENV === "test") {
  dbName = process.env.TEST_DB_NAME;
} else {
  dbName = process.env.PROD_DB_NAME;
}

(async () => {
  try {
    await mongoose.connect(`${process.env.DB_HOST_URL}/${dbName}`, options);
  } catch (error) {
    console.log("Database connection error: ", error);
  }
})();

const db = mongoose.connection;
if (process.env.NODE_ENV !== "test") {
  db.on("error", (error) => {
    console.log(
      "Error ocurred while establishing connection to the database: ",
      error
    );
  });

  db.on("open", () => {
    console.log("Connection to the database established successfully!");
  });
}

export default db;
