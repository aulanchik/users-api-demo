import mongoose, { Schema } from "mongoose";

export const UserSchema = new Schema({
  address: {
    type: String,
  },
  email: {
    required: true,
    type: String,
    unique: true,
  },
  name: {
    required: true,
    type: String,
    unique: true,
  },
});

export default mongoose.model("User", UserSchema);;
